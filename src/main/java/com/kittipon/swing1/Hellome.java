/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

    class MyActionlistener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }
        
    }
/**
 *
 * @author kitti
 */
public class Hellome implements ActionListener{
    public static void main(String[] args) {
        JFrame Myname = new JFrame("Hello me");
        Myname.setSize(500, 300);
        Myname.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel lblYourname = new JLabel("Your name");
        lblYourname.setSize(80,20);
        lblYourname.setLocation(5,5);
        lblYourname.setBackground(Color.WHITE);
        lblYourname.setOpaque(true);
        
        JTextField txtYourname = new JTextField();
        txtYourname.setSize(80,20);
        txtYourname.setLocation(90, 5);
        
        JButton btnYourname = new JButton("Hello");
        btnYourname.setSize(80,20);
        btnYourname.setLocation(90,40);
        
        MyActionlistener action = new MyActionlistener();
        btnYourname.addActionListener(action);
        btnYourname.addActionListener(new Hellome());
        
        ActionListener actionlistener = new ActionListener() { // this is anonymous class 
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous class : action");
            }
            
        };
        
        btnYourname.addActionListener(actionlistener);
        
        JLabel lblHello = new JLabel("Hello...",JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(90, 70);
        lblHello.setBackground(Color.YELLOW);
        lblHello.setOpaque(true);
        
        Myname.setLayout(null);
        
        Myname.add(lblYourname,JLabel.CENTER);
        Myname.add(txtYourname);
        Myname.add(btnYourname);
        Myname.add(lblHello);
        
        btnYourname.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourname.getText();
                lblHello.setText("Hello " + name);
            }
        });
        
        Myname.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {    
        System.out.println("Hellome : action");
    }
}
